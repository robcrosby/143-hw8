#!/usr/bin/env python
'''
Created on May 14, 2014
@author: reid

Modified on May 21, 2015
'''

import sys, nltk, operator, re
from nltk.stem.wordnet import WordNetLemmatizer
from operator import itemgetter
from qa_engine.base import QABase

driver = QABase()
stopwords = set(nltk.corpus.stopwords.words("english"))

def get_bow(tagged_tokens, stopwords):
    return set([t[0].lower() for t in tagged_tokens if t[0].lower() not in stopwords])

def find_phrase(tagged_tokens, qbow):
    for i in range(len(tagged_tokens) - 1, 0, -1):
        word = (tagged_tokens[i])[0]
        if word in qbow:
            return tagged_tokens[i+1:]

# The standard NLTK pipeline for POS tagging a document
def get_sentences(text):
    text_sentences = nltk.sent_tokenize(text)
    sentences = [nltk.word_tokenize(sent) for sent in text_sentences]
    sentences = [nltk.pos_tag(sent) for sent in sentences]
    return text_sentences,sentences

def builder(question, verbose=False):
    story = driver.get_story(question["sid"])
    story_text, story_tokens = get_sentences(story["text"])
    question_text, question_tokens = get_sentences(question["text"])

    keywords = [[word[0].lower() for word in sentence if word[0] not in stopwords] for sentence in question_tokens][0]
    for word in keywords:
        if re.match(".+ed$", word):
            new_word = list(word.replace("ed",""))
            keywords.append("".join(new_word[:-1]))
            new_word.append("e")
            keywords.append("".join(new_word))
            new_word = "".join(new_word[:-1])
            new_word = new_word + "ing"
            keywords.append(new_word)
    s_index = 0
    answer = [0,0]
    answers = []
    for sentence in story_tokens:
        match = 0
        for tuple in sentence:
            if tuple[0].lower() in keywords:
                if tuple[1] == "NN" or tuple[1] == "NNP" or tuple[1] == "NNS":
                    match+=5
                elif tuple[1] == "VBN" or tuple[1] == "VBD" or tuple[1] == "VBG" or tuple[1] == "VB":
                    match+=3
                else:
                    match+=1
        if match >= answer[0]:
            answers.append(story_text[s_index])
            answer = [match, s_index]
        s_index+=1
    if verbose:
        print(question_text)
        print(story_text[answer[1]])
    return story_text[answer[1]]

if __name__ == '__main__':
    test = False
    questions = driver.get_all_questions()
    if test:
        for id in questions:
            answer = builder(questions[id],True)
    else:
        with open('hw6-responses.tsv', mode='w') as csv_file:
            fieldnames = ['answer', 'qid']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
            for id in questions:
                answer = builder(questions[id])
                writer.writerow({'answer': answer, 'qid': id})
