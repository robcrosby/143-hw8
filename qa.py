from qa_engine.base import QABase
from qa_engine.score_answers import main as score_answers
import nltk, sys, operator
from sklearn.metrics.pairwise import cosine_similarity
from word2vec_extractor import Word2vecExtractor
from dependency_demo_stub import  find_main
import gensim
import re
from gensim.models import Word2Vec
from nltk import stem
from nltk.stem.wordnet import WordNetLemmatizer
import csv
from collections import defaultdict
from nltk.corpus import wordnet as wn

driver = QABase()
# The standard NLTK pipeline for POS tagging a document
lemmatizer = WordNetLemmatizer()
W2vecextractor = Word2vecExtractor("data/glove-w2v.txt")
stopwords = set(nltk.corpus.stopwords.words("english"))

DATA_DIR = "./wordnet"

def load_wordnet_ids(filename):
    file = open(filename, 'r')
    if "noun" in filename: type = "noun"
    else: type = "verb"
    csvreader = csv.DictReader(file, delimiter=",", quotechar='"')
    word_ids = defaultdict()
    for line in csvreader:
        word_ids[line['synset_id']] = {'synset_offset': line['synset_offset'], 'story_'+type: line['story_'+type], 'stories': line['stories']}
    return word_ids

noun_ids = load_wordnet_ids("{}/{}".format(DATA_DIR, "Wordnet_nouns.csv"))
verb_ids = load_wordnet_ids("{}/{}".format(DATA_DIR, "Wordnet_verbs.csv"))

#gets sentences and graphs
def text_type(type, story):
    if type == "sch":
        return nltk.sent_tokenize(story["sch"]), story["sch_dep"]
    else:
        return nltk.sent_tokenize((story["text"])), story["story_dep"]

def get_sentences(text):
    text_sentences = nltk.sent_tokenize(text)
    sentences = nltk.sent_tokenize(text)
    sentences= [nltk.word_tokenize(sent) for sent in sentences]
    sentences = [nltk.pos_tag(sent) for sent in sentences]

    return text_sentences, sentences


# gets rid of stop words after tokenization? So run through get_sentences then use get_bow
def get_bow(tagged_tokens, stopwords, question = False):
    if question is False:
        return set([t[0].lower() for t in tagged_tokens if t[0].lower() not in stopwords])
    else:
       # set([t[0].lower()
       s = []
       for m in tagged_tokens:
          for l in m:
            #print(m)
            if l[0].lower() not in stopwords:
                        s.append(l[0].lower())
       return set(s)



def find_phrase(tagged_tokens, qbow):
    for i in range(len(tagged_tokens) - 1, 0, -1):
        word = (tagged_tokens[i])[0]
        if word in qbow:
            return tagged_tokens[i + 1:]


# qtokens: is a list of pos tagged question tokens with SW removed
# sentences: is a list of pos tagged story sentences
# stopwords is a set of stopwords


def find_main(graph):
   for node in graph.nodes.values():
       #print(node)
       if node["rel"] == "root":
            return node
   return None

def find_root_what(graph):
   for node in graph.nodes.values():
      # print(node)
       if node["rel"] == "nmod":
            return node
   return None

def find_node(word, graph):
  #  print(graph, word)
    for node in graph.nodes.values():
        if node["word"] == word:
            return node
    return None


def get_dependents(node, graph):
    results = []
    for item in node["deps"]:
        address = node["deps"][item][0]
        #print(address)
        dep = graph.nodes[address]
       # print(dep)
        results.append(dep)
        #results = results + get_dependents(dep, graph
    return results

def find_sentence(word, sentences):
    i = 0
    #sgraph = None
    for graph in sentences:
       # print(graph)
        i += 1
        for node in graph.nodes.values():
            if node["word"] == word:
                return node, graph
       #print(i)
    return None, graph

def find_nouns(sentences):
    add = []
    dep = " "
    for node in sentences.nodes.values():
        if node["tag"] == "NNP"  :
           # print(node)
            m = list(node["deps"].keys())
            #print(node["word"])
            return node["word"].lower()
    return None

def why_qs(sentences, question):
    next = False
    results = []
    collect = [node["word"] for node in sentences.nodes.values()]
    if "not" in question or "because" in collect :
        for node in sentences.nodes.values():
            # print(node)
            if node['rel'] == "mark":
                 next = True
            if next is True:
                dep = node["address"]
                # print(dep)
                if node["word"] is not None:
                    results.append(node["word"])
        return " ".join(results)
    else:
        for node in sentences.nodes.values():
            if node['rel'] == "xcomp":
                        deps = get_dependents(node, sentences)
                        deps = sorted(deps + [node], key=operator.itemgetter("address"))
                        return " ".join(dep["word"] for dep in deps)

def what_qs(sgraph, qgraph):
    add = []
    dep = " "
    root = find_root_what(qgraph)
    if root is not None:
        for node in sgraph.nodes.values():
          #  print(node)
            if node["word"] == root["word"]:
                # print(node)
                 deps = get_dependents(node, sgraph)
                 deps = sorted(deps + [node], key=operator.itemgetter("address"))
                 # print(deps)
                 return " ".join(dep["word"] for dep in deps)
    return None


def find_answer(question, root, sgraph, snode, qgraph):
    if "Who" in question:
        nouns = find_nouns(sgraph)
        return nouns
 #   print(root, snode["word"])
    if "Why" in question:
        return why_qs(sgraph, question)
    if "feel" in root:
        return snode["word"]
    if "What" in question:
        return  what_qs(sgraph, qgraph )
    for node in sgraph.nodes.values():
            if node.get('head', None) == snode["address"]:
                if node['rel'] == "nmod":
                    deps = get_dependents(node, sgraph)
                    deps = sorted(deps + [node], key=operator.itemgetter("address"))
                    return " ".join(dep["word"] for dep in deps)

def baseline(question, sentences, stopwords, sgraph, root, qgraph):
    # Collect all the candidate answers
    story_text, story_tokens = get_sentences(sentences)
    question_text, question_tokens = get_sentences(question)
    keywords = [[word[0].lower() for word in sentence if word[0] not in stopwords] for sentence in question_tokens][0]
    for word in keywords:
        if re.match(".+ed$", word):
            new_word = list(word.replace("ed", ""))
            keywords.append("".join(new_word[:-1]))
            new_word.append("e")
            keywords.append("".join(new_word))
            new_word = "".join(new_word[:-1])
            new_word = new_word + "ing"
            keywords.append(new_word)
    s_index = 0
    answer = [0, 0]
    answers = []
    for num, sentence in enumerate(story_tokens):
        match = 0
        for tuple in sentence:
            if tuple[0].lower() in keywords:
                if tuple[1] == "NN" or tuple[1] == "NNP" or tuple[1] == "NNS":
                    match += 5
                elif tuple[1] == "VBN" or tuple[1] == "VBD" or tuple[1] == "VBG" or tuple[1] == "VB":
                    match += 3
                else:
                    match += 1
        if match >= answer[0]:
            answers.append(story_text[s_index])
            answer = [match, s_index]
        s_index += 1
    refine = find_answer(question, root, sgraph[answer[1]], find_main(sgraph[answer[1]]), qgraph)
    if refine is not None:
        return refine
    return story_text[answer[1]]

def baseline_word2vec_verb(question, sentences, stopwords, W2vecextractor, sgraphs, root, qgraph):
    q_feat = W2vecextractor.word2v(root)
    candidate_answers = []
    order = {}
    for i in range(0, len(sentences)):
        sent = sentences[i]
        s_verb = find_main(sgraphs[i])['lemma']
        s_verb =   lemmatizer.lemmatize(s_verb, 'v')
        a_feat = W2vecextractor.word2v(s_verb)

        dist = cosine_similarity([q_feat], [a_feat])

        candidate_answers.append((dist[0], sent))
        order[sent] = {"graph": sgraphs[i], "root":find_main(sgraphs[i])}
    answers = sorted(candidate_answers,  key=operator.itemgetter(0), reverse=True)
    best_answer = (answers[0])[1]
    refine = find_answer(question,root, order[best_answer]["graph"],order[best_answer]["root"], qgraph)
    if refine is not None:
        return refine
    return best_answer

def baseline_word2vec_sent(question, sentences, stopwords, W2vecextractor, sgraphs, root, qgraph):
     q_feat = W2vecextractor.sen2vec(question)
     candidate_answers = []
   #  print("question"+ str)
     order = {}
     for i in range(0, len(sentences)):
            sent = sentences[i]
            a_feat = W2vecextractor.sen2vec(sent)
            dist = cosine_similarity([q_feat], [a_feat])
            candidate_answers.append((dist[0], sent))
            order[sent] = i
     answers = sorted(candidate_answers, key=operator.itemgetter(0), reverse=True)
     best_answer = (answers[0])[1]
     num = order[best_answer]
     refine = find_answer(question, root, sgraphs[num], find_main(sgraphs[num]), qgraph)
     if refine is not None:
         return refine
     return best_answer

def wordnet(question,story):
    pos = nltk.pos_tag(nltk.word_tokenize(question))
    tags = ["NN","NNP","NNS","VBN","VBD","VBG","VB"]
    keywords = [tuple[0] for tuple in pos if tuple[1] in tags]
    for word in keywords:
        word_synset = wn.synsets(word)
        print(word)
        for rodent_synset in rodent_synsets:
            rodent_hypo = rodent_synset.hyponyms()
            print("%s: %s" % (rodent_synset, rodent_hypo))

            for hypo in rodent_hypo:
                print(hypo.name()[0:hypo.name().index(".")])
                print("is hypo_synset in Wordnet_nouns/verbs.csv?")
                # match on "mouse.n.01"


        # 'Know' is a hyponym of 'recognize' (know.v.09),
        # so we look at hypernyms of 'know' to find 'recognize'
        #
        # Question: What did the mouse know?
        # Answer: the voice of the lion
        # Sch: The mouse recognized the voice of the lion.
        know_synsets = wn.synsets("know")
        print("\n'Know' synsets: %s" % know_synsets)

        print("'Know' hypernyms")
        for know_synset in know_synsets:
            know_hyper = know_synset.hypernyms()
            print("%s: %s" % (know_synset, know_hyper))
        mirth_synsets = wn.synsets("express_mirth")
        print("\n'Express Mirth' synsets: %s" % mirth_synsets)

        print("'Express mirth' lemmas")
        for mirth_synset in mirth_synsets:
            print(mirth_synset)
            # look up in dictionary
            print("\n'%s' is in our dictionary: %s" % (mirth_synset.name(), (mirth_synset.name() in verb_ids)))


def get_answer(q, story):
    """
    :param question: dict
    :param story: dict
    :return: str


    question is a dictionary with keys:
        dep -- A list of dependency graphs for the question sentence.
        par -- A list of constituency parses for the question sentence.
        text -- The raw text of story.
        sid --  The story id.
        difficulty -- easy, medium, or hard
        type -- whether you need to use the 'sch' or 'story' versions
                of the .
        qid  --  The id of the question.


    story is a dictionary with keys:
        story_dep -- list of dependency graphs for each sentence of
                    the story version.
        sch_dep -- list of dependency graphs for each sentence of
                    the sch version.
        sch_par -- list of constituency parses for each sentence of
                    the sch version.
        story_par -- list of constituency parses for each sentence of
                    the story version.
        sch --  the raw text for the sch version.
        text -- the raw text for the story version.
        sid --  the story id


    """
    #figure out which sentence here

    type = q["type"]
    sents, sgraph = text_type(type, story)

    #texts for each
    text = story["text"]
    difficulty = q['difficulty']
    question = q["text"]
    print(difficulty + ": " + question)


    #Getting dependency graphs
    qgraph = q["dep"]
    q_verb = find_main(qgraph)["lemma"]
    wordnet(question,text)

    # answer3 = baseline_word2vec_sent(question, sents, stopwords, W2vecextractor, sgraph, q_verb, qgraph)
    if "Where" in question or "What" in question or "When" in question:
        return baseline(question, text, stopwords, root=q_verb, sgraph=sgraph, qgraph=qgraph)
    answer2 = baseline_word2vec_verb(question, sents, stopwords, W2vecextractor, sgraph, q_verb, qgraph)
    return answer2



#############################################################
###     Dont change the code in this section
#############################################################
class QAEngine(QABase):
    @staticmethod
    def answer_question(question, story):
        if question['difficulty'] == "Hard":
            print(story["text"] + "\n---")
            answer = get_answer(question, story)
            print(answer)
            return answer
        else:
            return ""


def run_qa():
    QA = QAEngine()
    QA.run()
    QA.save_answers()

#############################################################


def main():
    run_qa()
    # You can uncomment this next line to evaluate your
    # answers, or you can run score_answers.py
    # score_answers()

if __name__ == "__main__":
    main()
